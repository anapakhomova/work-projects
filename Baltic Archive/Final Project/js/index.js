// Top menu expansion function
let tablet_max_width = 992;

$(document).ready(function () {
    $(window).resize(function () {
        // Hide or show top menu depending on the size of the screen
        if ($(this).width() < tablet_max_width) {
            $('#top_menu').hide();
        } else {

            $('#top_menu').show();
        }
        // Hide or show button triggering expansion of the top menu on the size of the screen
        if ($(this).width() > tablet_max_width) {
            $('.menu-trigger').hide();
        } else {
            $('.menu-trigger').show();
        }
    });
    // Expand or collapse top menu + toggle icon class
    $('#toggle_menu').click(function () {
        $('#top_menu').toggle();
        $(this).find('i').toggleClass('fa-angle-double-down fa-angle-double-up');
    });
});

// Main menu functions

$(document).ready(function() {
    // Toggle small nav menu

    $('#btn_expand_nav_small').click(function(){
       $('#custom_nav_small').toggle();
    });
});

//  Description Section Circles Animation

$(document).ready(function () {
    $('.description-media').mouseover(function () {
        $(this).parent().find('.icon-circle').addClass('icon-circle-active');
        $(this).parent().find('h6').addClass('brand-color-active');
    });
    $('.description-media').mouseout(function () {
        $(this).parent().find('.icon-circle').removeClass('icon-circle-active', 'i-active-white');
        $(this).parent().find('h6').removeClass('brand-color-active');
    });
});

// Progress Bar Function

$(document).ready(function () {
    let check = true;
    let our_skills = $('#our_skills');
    let our_skills_offset = our_skills.offset();
    let our_skills_height = our_skills.outerHeight();

    $(window).scroll(function () {
        if (check && $(window).scrollTop() > our_skills_offset.top - our_skills_height) {
            $('.web-design').animate({width: '80%'}, 1000);
            $('.programming').animate({width: '60%'}, 1200);
            $('.html-css').animate({width: '70%'}, 1400);
            $('.wordpress').animate({width: '100%'}, 1600);
            $('.joomla').animate({width: '40%'}, 1800);

            setTimeout(function () {
                $('.web-design-title').addClass('title-animated');
            }, 500);
            setTimeout(function () {
                $('.programming-title').addClass('title-animated');
            }, 1000);
            setTimeout(function () {
                $('.html-css-title').addClass('title-animated');
            }, 1500);
            setTimeout(function () {
                $('.wordpress-title').addClass('title-animated');
            }, 2000);
            setTimeout(function () {
                $('.joomla-title').addClass('title-animated');
            }, 2500);

            check = false;

            // Running Numbers Animation

            (function ($) {
                $.fn.countTo = function (options) {
                    options = options || {};

                    return $(this).each(function () {
                        var settings = $.extend({}, $.fn.countTo.defaults, {
                            from: $(this).data('from'),
                            to: $(this).data('to'),
                            speed: $(this).data('speed'),
                            refreshInterval: $(this).data('refresh-interval'),
                            decimals: $(this).data('decimals')
                        }, options);

                        var loops = Math.ceil(settings.speed / settings.refreshInterval),
                            increment = (settings.to - settings.from) / loops;

                        var self = this,
                            $self = $(this),
                            loopCount = 0,
                            value = settings.from,
                            data = $self.data('countTo') || {};

                        $self.data('countTo', data);
                        if (data.interval) {
                            clearInterval(data.interval);
                        }
                        data.interval = setInterval(updateTimer, settings.refreshInterval);
                        render(value);

                        function updateTimer() {
                            value += increment;
                            loopCount++;
                            render(value);
                            if (typeof(settings.onUpdate) == 'function') {
                                settings.onUpdate.call(self, value);
                            }
                            if (loopCount >= loops) {
                                // remove the interval
                                $self.removeData('countTo');
                                clearInterval(data.interval);
                                value = settings.to;

                                if (typeof(settings.onComplete) == 'function') {
                                    settings.onComplete.call(self, value);
                                }
                            }
                        }

                        function render(value) {
                            var formattedValue = settings.formatter.call(self, value, settings);
                            $self.html(formattedValue);
                        }
                    });
                };
                $.fn.countTo.defaults = {
                    from: 0,
                    to: 0,
                    speed: 1000,
                    refreshInterval: 100,
                    decimals: 0,
                    formatter: formatter,
                    onUpdate: null,
                    onComplete: null
                };

                function formatter(value, settings) {
                    return value.toFixed(settings.decimals);
                }
            }(jQuery));

            jQuery(function ($) {
                // custom formatting example
                $('.count-number').data('countToOptions', {
                    formatter: function (value, options) {
                        return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
                    }
                });
                $('.timer').each(count);

                function count(options) {
                    var $this = $(this);
                    options = $.extend({}, options || {}, $this.data('countToOptions') || {});
                    $this.countTo(options);
                }
            });
        }
    });
});

// Line Expand Function ( #whySection)

$(document).ready(function () {
    $('#why_underline_one').mouseover(function () {
        $(this).find('.line-fill').addClass('line-filled');
    });
    $('#why_underline_one').mouseout(function () {
        $(this).find('.line-fill').removeClass('line-filled');
    });
    $('#why_underline_two').mouseover(function () {
        $(this).find('.line-fill').addClass('line-filled');
    });
    $('#why_underline_two').mouseout(function () {
        $(this).find('.line-fill').removeClass('line-filled');
    });
});

// Double-circle line expansion animation

$(document).ready(function () {
    $('.animate-header-underline').mouseover(function () {
        $(this).parent().find('.line-fill').addClass('line-filled');
    });
    $('.animate-header-underline').mouseout(function () {
        $(this).parent().find('.line-fill').removeClass('line-filled');
    });
});

//Double-icon line expansion animation

$(document).ready(function () {
    $('.animate-header-underline-icon').mouseover(function () {
        $(this).parent().find('.line-fill').addClass('line-filled');
    });
    $('.animate-header-underline-icon').mouseout(function () {
        $(this).parent().find('.line-fill').removeClass('line-filled');
    });
});

// Navtabs Images

$(document).ready(function () {
    $('#quality').parent().find('img').mouseover(function () {
        $(this).animate({'width': '110%'}, 400);
    });
    $('#quality').parent().find('img').mouseout(function () {
        $(this).animate({'width': '100%'}, 400);
    });
});

//  Accordion

$(document).ready(function () {
    $(".collapse.in").each(function () {
        $(this).siblings('.panel-heading').find('.glyphicon').addClass('glyphicon-minus').removeClass('glyphicon-plus');
    });

    $(".collapse").on('show.bs.collapse', function () {
        $(this).parent().find('.glyphicon').removeClass('glyphicon-plus').addClass('glyphicon-minus');
        $(this).parent().find('.panel-heading').addClass('panel-active');

    }).on('hide.bs.collapse', function () {
        $(this).parent().find('.glyphicon').removeClass('glyphicon-minus').addClass('glyphicon-plus');
        $(this).parent().find('.panel-heading').removeClass('panel-active');
    });
});


// Running Numbers Animation

$(document).ready(function () {
    let check = true;
    let run_number = $('#numerical_statistic');
    let run_number_offset = run_number.offset();
    let run_number_height = run_number.outerHeight();

    $(window).scroll(function () {
        if (check && $(window).scrollTop() > run_number_offset.top - (run_number_height + 400)) {
            setTimeout(function () {
                $('#number_run_one').addClass('running-number-active');
            }, 0);
            setTimeout(function () {
                $('#number_run_two').addClass('running-number-active');
            }, 300);
            setTimeout(function () {
                $('#number_run_three').addClass('running-number-active');
            }, 600);
            setTimeout(function () {
                $('#number_run_four').addClass('running-number-active');
            }, 900);
            check = false;

            // Running Numbers Animation

            (function ($) {
                $.fn.countTo = function (options) {
                    options = options || {};

                    return $(this).each(function () {
                        var settings = $.extend({}, $.fn.countTo.defaults, {
                            from: $(this).data('from'),
                            to: $(this).data('to'),
                            speed: $(this).data('speed'),
                            refreshInterval: $(this).data('refresh-interval'),
                            decimals: $(this).data('decimals')
                        }, options);

                        var loops = Math.ceil(settings.speed / settings.refreshInterval),
                            increment = (settings.to - settings.from) / loops;

                        var self = this,
                            $self = $(this),
                            loopCount = 0,
                            value = settings.from,
                            data = $self.data('countTo') || {};

                        $self.data('countTo', data);
                        if (data.interval) {
                            clearInterval(data.interval);
                        }
                        data.interval = setInterval(updateTimer, settings.refreshInterval);
                        render(value);

                        function updateTimer() {
                            value += increment;
                            loopCount++;
                            render(value);
                            if (typeof(settings.onUpdate) == 'function') {
                                settings.onUpdate.call(self, value);
                            }
                            if (loopCount >= loops) {
                                // remove the interval
                                $self.removeData('countTo');
                                clearInterval(data.interval);
                                value = settings.to;

                                if (typeof(settings.onComplete) == 'function') {
                                    settings.onComplete.call(self, value);
                                }
                            }
                        }

                        function render(value) {
                            var formattedValue = settings.formatter.call(self, value, settings);
                            $self.html(formattedValue);
                        }
                    });
                };
                $.fn.countTo.defaults = {
                    from: 0,
                    to: 0,
                    speed: 1000,
                    refreshInterval: 100,
                    decimals: 0,
                    formatter: formatter,
                    onUpdate: null,
                    onComplete: null
                };

                function formatter(value, settings) {
                    return value.toFixed(settings.decimals);
                }
            }(jQuery));

            jQuery(function ($) {
                // custom formatting example
                $('.count-number').data('countToOptions', {
                    formatter: function (value, options) {
                        return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
                    }
                });
                $('.timer').each(count);

                function count(options) {
                    var $this = $(this);
                    options = $.extend({}, options || {}, $this.data('countToOptions') || {});
                    $this.countTo(options);
                }
            });
        }
    });
});


// Team Icons Animation (#our_team section) + team member name

$(document).ready(function () {
    $('.icon-team').mouseover(function () {
        $(this).addClass('icon-team-active');
    });
    $('.icon-team').mouseout(function () {
        $(this).removeClass('icon-team-active');
    });
    $('.team-member').mouseover(function () {
        $(this).addClass('team-member-active');
    });
    $('.team-member').mouseout(function () {
        $(this).removeClass('team-member-active');
    });
});


// Our services animation

$(document).ready(function () {

    $('.our-service').mouseover(function () {
        $(this).find('h6').addClass('brand-color-active');
    });
    $('.our-service').mouseout(function () {
        $(this).find('h6').removeClass('brand-color-active');
    });
});

// Google Map Function

function regular_map() {
    var var_location = new google.maps.LatLng(40.725118, -73.997699);
    var var_mapoptions = {
        center: var_location,
        zoom: 14
    };
    var var_map = new google.maps.Map(document.getElementById("map_container"),
        var_mapoptions);
    var var_marker = new google.maps.Marker({
        position: var_location,
        map: var_map,
        title: "Our Location"
    });
}

// Initialize maps
google.maps.event.addDomListener(window, 'load', regular_map);


// Footer Animations

// Column 1 | Button " > Read more "
$(document).ready(function () {
    $('#colOneButtonReadMore').mouseover(function () {
        $(this).find('i').css('color', '#1ccdca');
    });
    $('#colOneButtonReadMore').mouseout(function () {
        $(this).find('i').css('color', 'white');
    });
});

// Column 1 | Button Arrows Animation
$(document).ready(function () {
    $('#btnArrowsAnimation').mouseover(function () {
        $(this).find('span').addClass('btn-arrows-mouseover');
        $(this).find('.arrow-active').show('slide', {direction: 'right'}, 500);

    });
    $('#btnArrowsAnimation').mouseout(function () {
        $(this).find('span').removeClass('btn-arrows-mouseover');
    });
});

// Column 3 | Cloud Tags

$(document).ready(function () {
    $('.cloud-tags').mouseover(function () {
        $(this).addClass('cloud-tags-change');
    });
    $('.cloud-tags').mouseout(function () {
        $(this).removeClass('cloud-tags-change');
    });
});

// Column 4 | Author's name + social media icons

$(document).ready(function () {
    $('.fa-twitter').mouseover(function () {
        $(this).addClass('brand-color-change');
    });
    $('.fa-twitter').mouseout(function () {
        $(this).removeClass('brand-color-change');
    });
    $('.fa-facebook-f').mouseover(function () {
        $(this).addClass('brand-color-change');
    });
    $('.fa-facebook-f').mouseout(function () {
        $(this).removeClass('brand-color-change');
    });
    $('.fa-google-plus-square').mouseover(function () {
        $(this).addClass('brand-color-change');
    });
    $('.fa-google-plus-square').mouseout(function () {
        $(this).removeClass('brand-color-change');
    });
    $('.fa-linkedin').mouseover(function () {
        $(this).addClass('brand-color-change');
    });
    $('.fa-linkedin').mouseout(function () {
        $(this).removeClass('brand-color-change');
    });
});



