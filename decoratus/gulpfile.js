let gulp = require('gulp');
let concatCss = require('gulp-concat-css');
let cleanCSS = require('gulp-clean-css');
let rename = require("gulp-rename");
let uncss = require('gulp-uncss');

gulp.task('default', function () {
    return gulp.src('css/*.css')
        .pipe(concatCss("styles/main.css"))
        /*
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(uncss({
            html: ['pages/*.html']
        }))
        */
        .pipe(rename("main.min.css"))
        .pipe(gulp.dest('out/'));
});