$(document).ready(function() {
    $('#navigation').load('./navigation.html');
    $('#footer').load('./footer.html');
});
$(document).ready(function() {
    $('#navigation_ru').load('../ru/navigation.html');
    $('#footer_ru').load('../ru/footer.html');
});
$(document).ready(function() {
    $('#navigation_eng').load('../eng/navigation.html');
    $('#footer_eng').load('../eng/footer.html');
});

$(document).ready(function () {

    //Menu solid background on scroll

    let cover_photo_height = $('.cover-img').height();
    let cover_photo_half = (cover_photo_height / 2) - 150;
    $(window).scroll(function () {
        let scroll = $(window).scrollTop();
        if (scroll > cover_photo_half) {
            $("#nav_menu").addClass('nav-menu-scrolled');
        }

        else {
            $("#nav_menu").removeClass('nav-menu-scrolled');
        }
    });

    // Info boxes appear on scroll animation

    let check = true;
    let fadein_box_section = $('.fadein-box-section');
    let fadein_box_section_offset = fadein_box_section.offset();
    let fadein_box_section_height = fadein_box_section.outerHeight();

    $(window).scroll(function () {
        if (check && $(window).scrollTop() > (fadein_box_section_offset.top + 100) - fadein_box_section_height) {
            setTimeout(function () {
                $('#fadein_box_one').addClass('box-active');
            }, 0);
            setTimeout(function () {
                $('#fadein_box_two').addClass('box-active');
            }, 300);
            setTimeout(function () {
                $('#fadein_box_three').addClass('box-active');
            }, 600);
            setTimeout(function () {
                $('#fadein_box_four').addClass('box-active');
            }, 900);
            check = false;
        }
    });
});

// Stop carousel
$('.carousel').carousel({
    interval: false
});


//Gallery Filter Buttons

$(document).ready(function() {
    $('a[href^="#"]').on('click', function(e) {

      e.preventDefault();

      let target = this.hash;
      let $target = $(target);

      //Scroll and show hash

        $('html, body').animate({
            'scrollTop': $target.offset().top
        }, 500, 'swing', function() {
            window.location.hash = target;
        });
    });
});

// Gallery button stop scroll when footer reached

$(document).ready(function() {
    let windw = this;

    $.fn.followTo = function ( pos ) {
        let $this = this,
            $window = $(windw);

        $window.scroll(function(e){
            if ($window.scrollTop() > pos) {
                $this.css({
                    position: 'absolute',
                    top: pos
                });
            } else {
                $this.css({
                    position: 'fixed',
                    top: 0
                });
            }
        });
    };

    $('#back_to_top').followTo(250);
});
