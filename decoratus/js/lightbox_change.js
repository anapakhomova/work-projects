// LightBox default settings change

lightbox.option({
    albumLabel: 'Photo %1 out of %2',
    'disableScrolling': false,
    'wrapAround': true
});

// LightBox disable on mobile

var lightboxOnResize = function lightboxOnResize() {
    if ($(window).width() > 800) {
        $('a[rel="data-lightbox"]')
            .removeProp('rel')
            .addClass('lightboxRemoved');
    } else {
        $('a.lightboxRemoved').prop('rel', 'data-lightbox');
    }
};

$(document).ready(lightboxOnResize);
$(window).resize(lightboxOnResize);

